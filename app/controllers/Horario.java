
package controllers;

import models.Horarios;
import models.Personas;
import play.mvc.Controller;

/**
 *
 * @author Alets
 */
public class Horario extends Controller {
    public  void fillHorario(){
        Personas cristina = (Personas) Personas.find("Nombre = ?", "Cristina").fetch().get(0);
        Personas milton = (Personas) Personas.find("Nombre = ?","Milton Joel").fetch().get(0);
        Personas oscar = (Personas) Personas.find("Nombre = ?","Óscar").fetch().get(0);
        Personas chaparro = (Personas) Personas.find("Nombre = ?","Jorge Alberto").fetch().get(0);
        Personas cortes = (Personas) Personas.find("Nombre = ?","Francisco").fetch().get(0);
        Personas eli = (Personas) Personas.find("Nombre = ?","Juan Elí").fetch().get(0);
        Personas addi = (Personas) Personas.find("Nombre = ?","Addai").fetch().get(0);
        Personas evelyn = (Personas) Personas.find("Nombre = ?","Evelyn Paulina").fetch().get(0);
        Personas alfonso = (Personas) Personas.find("Nombre = ?","Alfonso José").fetch().get(0);
        Personas dynhora = (Personas) Personas.find("Nombre = ?","Dynhora").fetch().get(0);
   
    Horarios h[] = {new Horarios(cristina,1,2),
                    new Horarios(milton,3,2),new Horarios(milton,4,2),new Horarios(milton,5,2),
                    new Horarios(oscar,2,2),new Horarios(oscar,4,2),
                    new Horarios(chaparro,2,2),new Horarios(chaparro,4,10),
                    new Horarios(cortes,1,2),new Horarios(cortes,3,2),new Horarios(cortes,4,12),new Horarios(cortes,5,11),
                    new Horarios(eli,4,12),new Horarios(eli,5,2),
                    new Horarios(addi,1,1),new Horarios(addi,1,2),new Horarios(addi,2,2),
                    new Horarios(evelyn,3,2),
                    new Horarios(alfonso,2,2),new Horarios(alfonso,2,3),new Horarios(alfonso,4,2),new Horarios(alfonso,4,3),
                    new Horarios(dynhora,2,2),new Horarios(dynhora,2,3)};
        for (Horarios h1 : h) {
            h1.create();
        }
    
    }
    
}
