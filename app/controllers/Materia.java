/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import models.Materias;
import play.mvc.Controller;

/**
 *
 * @author Jalil
 */
public class Materia extends Controller{
 
    /**
    * Inserta datos prueba a la tabla de mateias con informacion del edificio de Tics a la base de datos.
    */
    public  void fillDatabaseMaterias(){
        Materias FSC = new Materias("FSC01","Formación Sociocultural I");
        Materias FSC2 = new Materias("FSC02","Formación Sociocultural II");
        Materias FSC3 = new Materias("FSC03","Formación Sociocultural III");
        Materias FSC4 = new Materias("FSC04","Formación Sociocultural IV");
        
        Materias MDLP = new Materias("MDLP01","Metodología de la Programación");
        Materias P = new Materias("P01","Programación");
        
        Materias DDA = new Materias("DDA01","Desarrollo de Aplicaciones I");
        Materias DDA2 = new Materias("DDA02","Desarrollo de Aplicaciones II");
        Materias DDA3 = new Materias("DDA03","Desarrollo de Aplicaciones III");
        
        Materias PDA = new Materias("PDA01","Programación de Aplicaciones");
        
        Materias EDD = new Materias("EDD01","Estructura de Datos");
        
        Materias BDD = new Materias("BDD01","Base de Datos I");
        Materias BDD2 = new Materias("BDD02","Base de Datos II");
        Materias ADBDD = new Materias("ADBDD01","Administración de Base de Datos");
        
        Materias DDSW = new Materias("DDSW01","Desarrollo de Sitios Web");
        Materias BDDPA = new Materias("BDDPA01","Base de Datos para Aplicaciones");
                
        Materias DG = new Materias("DG01","Diseño Gráfico");
        
        Materias DHPL = new Materias("DHPL01","Desarrollo de Habilidades del Pensamiento Lógico");
        Materias DHPM = new Materias("DHPM01","Desarrollo de Habilidades del Pensamiento Matemático");
        
        Materias MPT = new Materias("MPT01","Matemáticas para Telecomunicaciones");
        Materias MPTI = new Materias("MPTI01","Matemáticas para TI");
        
        Materias IE = new Materias("IE01","Ingenieria Económica");
        Materias EA = new Materias("EA01","Estadística Aplicada");
        
        Materias FR = new Materias("FR01","Fundamentos de Redes");
        Materias RAL = new Materias("RAL01","Redes de Área Local");
        Materias CRD = new Materias("CRD01","Conmutación de Redes de Datos");
        Materias RW = new Materias("RW01","Redes WAN");
        Materias RLW = new Materias("RLW01","Redes LAN y WAN");
        Materias RC = new Materias("RC01","Redes Convergentes");
        Materias SI = new Materias("SI01","Seguridad de la Información");
        Materias AT = new Materias("AT01","Aplicación de las Telecomunicaciones");
        
        Materias ST = new Materias("ST01","Soporte Técnico");
        
        Materias AP = new Materias("AP01","Administración de Proyectos");
        Materias I = new Materias("I01","Integradora I");
        Materias I2 = new Materias("I02","Integradora II");
        
        Materias DSWCE = new Materias("DSWCE01","Desarrollo de Sitios Web para Comercio Electrónico");
        
        Materias IS = new Materias("IS01","Ingeniería de Software I");
        Materias IS2 = new Materias("IS02","Ingeniería de Software II");

        Materias MPN = new Materias("MPN01","Modelado de Proceso de Negocios");
        Materias O = new Materias("O01","Ofimática");
        
        FSC.create(); 
        FSC2.create();
        FSC3.create();
        FSC4.create();
        MDLP.create();
        P.create();
        DDA.create(); 
        DDA2.create();
        DDA3.create();
        PDA.create(); 
        EDD.create(); 
        BDD.create(); 
        BDD2.create(); 
        ADBDD.create();
        BDDPA.create();
        DDSW.create(); 
        DG.create(); 
        DHPL.create(); 
        DHPM.create(); 
        MPT.create();
        MPTI.create();
        IE.create(); 
        EA.create(); 
        FR.create(); 
        RAL.create(); 
        CRD.create(); 
        RW.create(); 
        RLW.create();
        RC.create(); 
        RLW.create(); 
        SI.create();  
        AT.create(); 
        ST.create();
        AP.create(); 
        I.create(); 
        I2.create(); 
        DSWCE.create();
        IS.create(); 
        IS2.create(); 
        MPN.create();
        O.create(); 
        
    }
     /**
      * Manda a la view una lista de todas las materias.
      */
     public void getAll(){
         render(Materias.findAll());
     }
    
}
