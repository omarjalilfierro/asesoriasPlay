/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import play.mvc.Controller;

/**
 *
 * @author Alets
 */
public class Todo extends Controller {
    
    public static void fillDatabase(){
        
       Persona p = new Persona();
               p.fillPersonas();
        p.fillDatabaseAlumnos();
         Materia m = new Materia();
         m.fillDatabaseMaterias();
        Horario h = new Horario();
                h.fillHorario();
    HistorialDocente  hd = new HistorialDocente();
            hd.fillHistorial();
        
        
        renderText("Base de datos llenada");
    }
}
