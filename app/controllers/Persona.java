/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.Date;
import java.util.Random;
import models.Alumnos;
import models.Carreras;
import models.Estatus;
import models.Grupos;
import models.Periodos;
import models.Personas;
import play.mvc.Controller;

/**
 *
 * @author Jalil
 */
public class Persona extends Controller{
    Random a = new Random();
    
    
    String[] nombres = { "JUAN", "JOSÉ LUIS", "JOSÉ",
            "MARÍA GUADALUPE",
            "FRANCISCO","GUADALUPE",
            "MARÍA",
            "JUANA",
            "ANTONIO",
            "JESÚS",
            "MIGUEL ÁNGEL",
            "PEDRO",
            "ALEJANDRO",
            "MANUEL",
            "MARGARITA",
            "MARÍA DEL CARMEN",
            "JUAN CARLOS",
            "ROBERTO",
            "FERNANDO",
            "DANIEL",
            "CARLOS",
            "JORGE",
            "RICARDO",
            "MIGUEL",
            "EDUARDO",
            "JAVIER",
            "RAFAEL",
            "MARTÍN"};
    String [] apellidos = {
    "González", "Muñoz","Rojas","Díaz","Pérez","Soto","Contreras",
            "Silva",
            "Martínez",
            "Sepúlveda",
            "Morales",
            "Rodríguez",
            "López",
            "Fuentes",
            "Hernández",
            "Torres",
            "Araya",
            "Flores",
            "Espinoza",
            "Valenzuela",
            "Castillo",
            "Ramírez",
            "Reyes",
            "Gutiérrez",
            "Castro",
            "Vargas",
            "Álvarez",
            "Vásquez",
            "Tapia",
            "Fernández",
            "Sánchez",
            "Carrasco",
            "Gómez",
            "Cortés",
            "Herrera",
            "Núñez",
            "Jara",
            "Vergara",
            "Rivera",
            "Figueroa",
            "Riquelme",
            "García",
            "Miranda",
            "Bravo",
            "Vera",
            "Molina",
            "Vega",
            "Campos",
            "Sandoval",
    };
    
    
    /**
     * Llena la base de datos con carreras, estatus, periodos y grupos
     */
    public static void fillDatabaseProfesores(){
        
        Carreras c = new Carreras("Teconologias de la informacion");
        c.create();
        Estatus e = new Estatus("activo");
        e.create();
        Periodos p = new Periodos(1l, 2018, e , "ene-abril2018",new Date (27,01,2018), new Date (27,04,2018));
        p.create();
        Random r = new Random();
        Grupos g =new Grupos(""+r.nextInt(),p,c);
        g.create();
    }
    
    /**
     * Inserta los datos de los principales docentes de la carrera
     */
     public  void  fillPersonas(){
        Personas cristina = new Personas("Cristina","Barba","Martinez");
        Personas milton = new Personas("Milton Joel","Batres","Márquez");
        Personas oscar = new Personas("Óscar","Beltrán","Gómez");
        Personas chaparro = new Personas("Jorge Alberto","Chaparro","Tarango");
        Personas cortes = new Personas("Francisco","Cortés","Carrilloz");
        Personas eli = new Personas("Juan Elí","Gomez","Vidal");
        Personas addi = new Personas("Addai","Guerrero","Quiñones");
        Personas evelyn = new Personas("Evelyn Paulina","Hinojos","Cepeda");
        Personas alfonso = new Personas("Alfonso José","Barroso","Barajas");
        Personas dynhora = new Personas("Dynhora","Danheyda","Ramirez");
        
        cristina.create();
        milton.create();
        oscar.create();
        chaparro.create();
        cortes.create();
        eli.create();
        addi.create();
        evelyn.create();
        alfonso.create();
        dynhora.create();     
    }
    
    /**
     * Genera 100 alumnos con los nombres y apellidos más comunes de America Latina C: y los inserta en la base de datos
     */
    public  void fillDatabaseAlumnos(){
        //crea 100 alumnos random 
        for (int i = 0; i<100; i++){
            Alumnos a = new Alumno().createAlumnoRandom();
            a.create();
        }
    }
    
    /**
     * Toma un nombre y dos apellidos para unirlos y regresaar una persona
     * @return Retorna un objeto Personas que incluye nombre y apellidos
     */
     public  Personas createPersonaRandom(){
        return new Personas(nombres[a.nextInt(nombres.length)],apellidos[a.nextInt(apellidos.length)],apellidos[a.nextInt(apellidos.length)]);   
    }
}
