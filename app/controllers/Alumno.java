package controllers;

import java.util.ArrayList;
import java.util.Random;
import models.Alumnos;
import models.Materias;
import models.Personas;
import play.mvc.*;

public class Alumno extends Controller {

    Random r = new Random();
    
    
  /**
   * Crea un alumno con nombre random  lo inserta en la base de datos.
   * @return El alumno creado.
   */
    public Alumnos createAlumnoRandom(){
        Personas p = new Persona().createPersonaRandom();
        p.create();
        return new Alumnos(p,""+r.nextInt());
    }
    
    
    
    public static void index(){
        render();
    }
    

}
