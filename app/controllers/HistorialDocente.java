package controllers;

import java.util.Date;
import java.util.Random;
import models.Carreras;
import models.Estatus;
import models.Grupos;
import models.Historial_Docente;
import models.Materias;
import models.Periodos;
import models.Personas;
import play.mvc.Controller;

/**
 *
 * @author Jalil
 */
public class HistorialDocente extends Controller {
    
    
   /**
    * Inserta datos prueba con informacion del edificio de Tics a la base de datos.
    */
    public void fillHistorial(){
        
    Personas cristina = (Personas) Personas.find("Nombre = ?", "Cristina").fetch().get(0);
    Personas milton = (Personas) Personas.find("Nombre = ?","Milton Joel").fetch().get(0);
    Personas oscar = (Personas) Personas.find("Nombre = ?","Óscar").fetch().get(0);
    Personas chaparro = (Personas) Personas.find("Nombre = ?","Jorge Alberto").fetch().get(0);
    Personas cortes = (Personas) Personas.find("Nombre = ?","Francisco").fetch().get(0);
    Personas eli = (Personas) Personas.find("Nombre = ?","Juan Elí").fetch().get(0);
    Personas addi = (Personas) Personas.find("Nombre = ?","Addai").fetch().get(0);
    Personas evelyn = (Personas) Personas.find("Nombre = ?","Evelyn Paulina").fetch().get(0);
    Personas alfonso = (Personas) Personas.find("Nombre = ?","Alfonso José").fetch().get(0);
    Personas dynhora = (Personas) Personas.find("Nombre = ?","Dynhora").fetch().get(0);
     
    Materias FSC = Materias.find("Clave = ?","FSC01").first();
        Materias FSC2 = Materias.find("Clave = ?","FSC02").first();
        Materias FSC3 = Materias.find("Clave = ?","FSC03").first();
        Materias FSC4 = Materias.find("Clave = ?","FSC04").first();
        Materias MDLP = Materias.find("Clave = ?","MDLP01").first();
        Materias P = Materias.find("Clave = ?","P01").first();
        Materias DDA = Materias.find("Clave = ?","DDA01").first();
        Materias DDA2 = Materias.find("Clave = ?","DDA02").first();
        Materias DDA3 = Materias.find("Clave = ?","DDA03").first();
        Materias PDA = Materias.find("Clave = ?","PDA01").first();
        Materias EDD = Materias.find("Clave = ?","EDD01").first();
        Materias BDD = Materias.find("Clave = ?","BDD01").first();
        Materias BDD2 = Materias.find("Clave = ?","BDD02").first();
        Materias ADBDD = Materias.find("Clave = ?","ADBDD01").first();
        Materias DDSW = Materias.find("Clave = ?","DDSW01").first();
        Materias BDDPA = Materias.find("Clave = ?","BDDPA01").first();
        Materias DG = Materias.find("Clave = ?","DG01").first();
        Materias DHPL = Materias.find("Clave = ?","DHPL01").first();
        Materias DHPM = Materias.find("Clave = ?","DHPM01").first();
        Materias MPT = Materias.find("Clave = ?","MPT01").first();
        Materias MPTI = Materias.find("Clave = ?","MPTI01").first();
        Materias IE = Materias.find("Clave = ?","IE01").first();
        Materias EA = Materias.find("Clave = ?","EA01").first();
        Materias FR = Materias.find("Clave = ?","FR01").first();
        Materias RAL = Materias.find("Clave = ?","RAL01").first();
        Materias CRD = Materias.find("Clave = ?","CRD01").first();
        Materias RW = Materias.find("Clave = ?","RW01").first();
        Materias RLW = Materias.find("Clave = ?","RLW01").first();
        Materias RC = Materias.find("Clave = ?","RC01").first();
        Materias SI = Materias.find("Clave = ?","SI01").first();
        Materias AT = Materias.find("Clave = ?","AT01").first();
        Materias ST = Materias.find("Clave = ?","ST01").first();
        Materias AP = Materias.find("Clave = ?","AP01").first();
        Materias I = Materias.find("Clave = ?","I01").first();
        Materias I2 = Materias.find("Clave = ?","I02").first();
        Materias IS = Materias.find("Clave = ?","IS01").first();
        Materias IS2 = Materias.find("Clave = ?","IS02").first();
        Materias MPN = Materias.find("Clave = ?","MPN01").first();
        Materias O = Materias.find("Clave = ?","O01").first();
        Materias DSWCE = Materias.find("Clave = ?","DSWCE01").first();


        Estatus e =  Estatus.find("Estatus = ?","activo").first();
     
        Periodos p = Periodos.find("Nombre = ?","ene-abril2018").first();
    
        Grupos g =Grupos.find("Periodo = ?", p).first();
        
        Historial_Docente hd[] ={
            
            new Historial_Docente(cristina,FSC, p, g, e),
            new Historial_Docente(cristina,FSC2, p, g, e),
            new Historial_Docente(cristina,FSC3, p, g, e),
            new Historial_Docente(cristina,FSC4, p, g, e),
            new Historial_Docente(milton,MDLP, p, g, e),
            new Historial_Docente(milton,P, p, g, e),
            new Historial_Docente(milton,DDA, p, g, e),
            new Historial_Docente(milton,DDA2, p, g, e),
            new Historial_Docente(milton,DDA3, p, g, e),
            new Historial_Docente(milton,PDA, p, g, e),
            new Historial_Docente(milton,BDD2, p, g, e),
            new Historial_Docente(milton,EDD, p, g, e),
             new Historial_Docente(oscar,MDLP, p, g, e),
             new Historial_Docente(oscar,P, p, g, e),
             new Historial_Docente(oscar,BDD, p, g, e),
             new Historial_Docente(oscar,BDD2, p, g, e),
             new Historial_Docente(oscar,DDA, p, g, e),
             new Historial_Docente(oscar,DDA2, p, g, e),
             new Historial_Docente(oscar,DDA3, p, g, e),
             new Historial_Docente(oscar,DDSW, p, g, e),
             new Historial_Docente(oscar,PDA, p, g, e),
             new Historial_Docente(oscar,BDDPA, p, g, e),
             new Historial_Docente(oscar,DSWCE, p, g, e),
             new Historial_Docente(chaparro,MDLP, p, g, e),
             new Historial_Docente(chaparro,P, p, g, e),
             new Historial_Docente(chaparro,BDD, p, g, e),
             new Historial_Docente(chaparro,BDD2, p, g, e),
             new Historial_Docente(chaparro,ADBDD, p, g, e),
             new Historial_Docente(chaparro,PDA, p, g, e),
             new Historial_Docente(chaparro,BDDPA, p, g, e),
             new Historial_Docente(chaparro,DG, p, g, e),
             new Historial_Docente(cortes,DHPL, p, g, e),
             new Historial_Docente(cortes,DHPM, p, g, e),
             new Historial_Docente(cortes,MPT, p, g, e),
             new Historial_Docente(cortes,MPTI, p, g, e),
             new Historial_Docente(cortes,IE, p, g, e),
             new Historial_Docente(cortes,EA, p, g, e)
        };
        
        for (Historial_Docente h1 : hd) {
            h1.create();
        }
        
    }
    
}
