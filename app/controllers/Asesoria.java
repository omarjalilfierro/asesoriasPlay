/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import models.Alumnos;
import models.Asesorias;
import models.Historial_Docente;
import models.Horarios;
import models.Materias;
import models.Personas;
import play.mvc.Controller;

/**
 *
 * @author Jalil
 */
public class Asesoria extends Controller {

    /**
     * Regresa todos los alumnos que se encuentran en la base de datos 
     * y los envia al HTML de Asesoria para mostrarlos como texto.
     */
    public void mostrarAlumnos(){
        List<Alumnos> alumnos  = Alumnos.findAll();
        
        renderText(alumnos);
        //Asesorias a = new Asesorias((ArrayList)alumnos, ,new Date(10,10,10),"puta");
        //a.create();      
    }
    
    /**
     * pedirAsesoria busca en la base de datos con los criterios de entrada de 
     * profesor y fecha de asesoria. 
     * Regresa el primer resultado que encuentra y despues lo inserta en un 
     * arraylist para despues insertarlo en la base de datos 
     * @param alumno Alumno que pide la asesoria
     * @param profesor Profesor de la materia que se busca
     * @param fecha Fecha de la asesoria deseada
     */
    public void pedirAsesoria(Alumnos alumno,Personas profesor,Materias materia,Date fecha){
        Asesorias asesoria = Asesorias.find("Profesor = ? && Fecha = ?", profesor,fecha).first();
        if(asesoria == null){
            asesoria = new Asesorias(new ArrayList<Alumnos>(), profesor,materia, fecha, "");
            asesoria.addAlumno(alumno);
            asesoria.create();
        }
        else{
           asesoria.addAlumno(alumno);
            asesoria.save(); 
        }
    }
     
    /**
     * Trae la primera asesoria de la base de datos y manda los datos al HTML para que puedan 
     * ser mostrados como texto.
     */
    public void mostrarPrimeraAsesoria(){
        Asesorias a = Asesorias.findById(1l);
        renderText(a.getLista());
    }
    
    /**
     * Esta funcion se llama desde routes con el path de /buscar con el cual se busca la materia por clave
     * En este caso se busca la materia "P01" (programación 1) en la base de datos y se regresan todos los docentes
     * que imparten dicha materia.
     * Luego se buscan los horarios en la base de datos con el criterio del docente.
     * Al final manda la información al HTML del nombre del docente, que dias imparte asesorias y a que horas. 
     */
    public void index(){
        String text= "";
        Materias P = Materias.find("Clave = ?","P01").first();
        text+= P.getClave()+"\n";
        List<Historial_Docente> maestros = Historial_Docente.find("Materia = ?",P).fetch();
        ArrayList<List>horarios = new ArrayList();
        
        for(Historial_Docente hd : maestros){
          String nombre = hd.Docente.Nombre;
           text+= nombre+"\n";
           
           List<Horarios> horario = Horarios.find("docente = ?",hd.Docente).fetch();
           horarios.add(horario);
           
           for(Horarios h :horario){
               text+= h.getTextDay()+" ";
           } 
           text+="\n";
       } 
       render(horarios);
       
    }
    
    
    
    /**
     * Renderiza todas las asesorias de un año 
     * @param anio año a buscar 
     */
     public void getPorAnio(int anio){
         List<Asesorias> asesorias =  Asesorias.find("Anio= ? order by Anio", anio).fetch();
         render(asesorias);
    }
     /**
       * Renderiza todas las asesorias de un mes 
     * @param anio año a buscar 
     * @param mes  mes a buscar
      */
    public void getPorMes(int anio, int mes){
         List<Asesorias> asesorias =  Asesorias.find("Mes = ? && Anio= ? order by Anio,Mes", mes,anio).fetch();
         render(asesorias);
    }
    /**
     * Renderiza todas las asesorias de un dia 
     * @param anio año a buscar 
     * @param mes  mes a buscar
     * @param dia  dia a buscar
     */
    public void getPorDia(int anio, int mes,int dia){
         List<Asesorias> asesorias =  Asesorias.find("Mes = ? && Anio= ? && Dia = ? order by Anio,Mes" , mes,anio,dia).fetch();
         render(asesorias);
    }
   
    public void getPorMaestro(Personas docente){
         List<Asesorias> asesorias =  Asesorias.find("Profesor = ? order by Profesor", docente).fetch();
         render(asesorias);
    }
    public void getPorMateria(Materias materia){
         List<Asesorias> asesorias =  Asesorias.find("Materia = ? order by Materia", materia).fetch();
         render(asesorias);
    }
    
}
