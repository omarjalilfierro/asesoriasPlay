package models;

import javax.persistence.Entity;
import play.db.jpa.Model;

/**
 *
 * Catalogo de Materias
 *
 * @author Administración de Proyectos Informáticos
 * @version 1.0
 * @since 2011
 * @category Catalogo
 */
@Entity
public class Materias extends Model {

    private String Clave;
    private String Materia;

    /**
     * 
     * @param Clave     Identificador de la materia
     * @param Materia   Nombre de la materia
     */
    public Materias(String Clave, String Materia) {
        this.Clave = Clave;
        this.Materia = Materia;
    }
    
    public Personas ProfesorImparte(Long periodo_id, Long alumno_id) {
        /*Retornar el profesor que imparte la materia*/
        Personas docente = Personas.findById(1L);
        return docente;
    }

    public String getClave() {
        return Clave;
    }

    public String getMateria() {
        return Materia;
    }

    public void setClave(String Clave) {
        this.Clave = Clave;
    }

    public void setMateria(String Materia) {
        this.Materia = Materia;
    }
    
    
}
