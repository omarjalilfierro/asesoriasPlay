package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import play.db.jpa.Model;

@Entity
public class Planes_estudio extends Model {

    @Column(name = "Año")
    public Integer anio;

    /**
     * 
     * @param anio Año del plan de estudios
     */
    public Planes_estudio(Integer anio) {
        this.anio = anio;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }
    
    

}
