/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import play.db.jpa.Model;

/**
 *
 * @author Jalil
 */
@Entity
public class Horarios extends Model{
    
    @OneToOne
    private Personas docente;
    private int hourBegin;
    private int day;

    /**
     * 
     * @param docente       Maestro en la base de datos
     * @param day           Dia que imparte dicha asesoria
     * @param hourBegin     A que hora empieza dicha asesoria
     */
    public Horarios(Personas docente, int day,int hourBegin) {
        this.docente = docente;
        this.hourBegin = hourBegin;
        this.day = day;
    }
    
    /**
     * En base al numero del dia de la semana regresa el string de dicho dia
     * @return String del numero del dia
     */
    public String getTextDay(){
        switch (day){
            case 1:
                return "Lunes";
                case 2:
                return "Martes";
                case 3:
                return "Miercoles";
                case 4:
                return "Jueves";
                case 5:
                return "Viernes";
                
        }
        return "";
    }

    public String getDocente() {
        return docente.Nombre;
    }

    public void setDocente(Personas docente) {
        this.docente = docente;
    }

    public int getHourBegin() {
        return hourBegin;
    }

    public void setHourBegin(int hourBegin) {
        this.hourBegin = hourBegin;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
    
    
}
