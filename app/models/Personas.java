package models;

import javax.persistence.*;
import play.data.validation.*;
import play.db.jpa.Model;

@Entity
public class Personas extends Model {

    @Required
    public String Nombre;
    @Required
    public String Ap_paterno;
    @Required
    public String Ap_materno;
    
    /**
     * 
     * @param Nombre        Nombre de la persona
     * @param Ap_paterno    Apellido paterno   
     * @param Ap_materno    Apellido materno
     */
    public Personas(String Nombre, String Ap_paterno, String Ap_materno) {
        this.Nombre = Nombre;
        this.Ap_paterno = Ap_paterno;
        this.Ap_materno = Ap_materno;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getAp_paterno() {
        return Ap_paterno;
    }

    public void setAp_paterno(String Ap_paterno) {
        this.Ap_paterno = Ap_paterno;
    }

    public String getAp_materno() {
        return Ap_materno;
    }

    public void setAp_materno(String Ap_materno) {
        this.Ap_materno = Ap_materno;
    }

    
    
}
