/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;
import java.util.Date;
import javax.persistence.Entity;
import play.db.jpa.Model;

/**
 *
 * @author Alets
 */
@Entity
public class Asesorias extends Model {
    
    ArrayList<Alumnos> Lista;
    Personas Profesor;
    Materias Materia;
    int Mes ,Dia,Anio;
    String Comentario;
/**
 * Genera un objeto asesorias default
 * @param Lista Alumnos que cursaron la asesoria.
 * @param Profesor Preofesor que impartio la asesoria.
 * @param Fecha 
 * @param Comentario Observaciones de el elvento. 
 */
    public Asesorias(ArrayList<Alumnos> Lista ,Personas Profesor ,Materias Materia, Date Fecha, String Comentario) {
        this.Lista = Lista;
        this.Profesor = Profesor;
        this.Materia = Materia;
        this.Dia = Fecha.getDay();
        this.Mes = Fecha.getMonth();
        this.Anio = Fecha.getYear();
        this.Comentario = Comentario;
    }
    /**
     * 
     * @return Todos los alumnos que asisitieron.
     */
    public ArrayList<Alumnos> getLista(){
        return Lista;
    }
    /**
     * Añade un alumno a la lista de asistencia.
     * @param alumno 
     */
    public void addAlumno(Alumnos alumno){
        Lista.add(alumno);  
        this.save();
    }
    
    
    
}
