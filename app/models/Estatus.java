package models;

import javax.persistence.Entity;
import play.db.jpa.Model;

/**
 * 
 * activo, no activo
 * 
 * @author Administracion de Proyectos Informaticos
 * @since 2011
 * @version 1.0
 * @category Sistema
 */
@Entity
public class Estatus extends Model {
    public String Estatus;
/**
 * ...
 * @param Estatus ...
 */
    public Estatus(String Estatus) {
        this.Estatus = Estatus;
    }
    
    
}
