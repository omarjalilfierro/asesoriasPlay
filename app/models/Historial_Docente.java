package models;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import play.db.jpa.Model;

@Entity
public class Historial_Docente extends Model {

    @OneToOne
    public Personas Docente;

    @OneToOne
    private Materias Materia;

    @OneToOne
    private Periodos Periodo;

    @OneToOne
    private Grupos Grupo;

    @OneToOne
    private Estatus Estatus;

    /**
     * 
     * @param Docente Representa a los maestros en la base de datos
     * @param Materia Representa las materias impartidas por el maestro
     * @param Periodo Periodo de clases actual
     * @param Grupo   Grupos a los que imparte clase
     * @param Estatus Estatus QUO
     */
    public Historial_Docente(Personas Docente, Materias Materia, Periodos Periodo, Grupos Grupo, Estatus Estatus) {
        this.Docente = Docente;
        this.Materia = Materia;
        this.Periodo = Periodo;
        this.Grupo = Grupo;
        this.Estatus = Estatus;
    }
    
    public Personas getDocente() {
        return Docente;
    }

    public void setDocente(Personas Docente) {
        this.Docente = Docente;
    }

    public Materias getMateria() {
        return Materia;
    }

    public void setMateria(Materias Materia) {
        this.Materia = Materia;
    }

    public Periodos getPeriodo() {
        return Periodo;
    }

    public void setPeriodo(Periodos Periodo) {
        this.Periodo = Periodo;
    }

    public Grupos getGrupo() {
        return Grupo;
    }

    public void setGrupo(Grupos Grupo) {
        this.Grupo = Grupo;
    }

    public Estatus getEstatus() {
        return Estatus;
    }

    public void setEstatus(Estatus Estatus) {
        this.Estatus = Estatus;
    }

    
    
    
    
}
