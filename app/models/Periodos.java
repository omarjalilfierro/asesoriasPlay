package models;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import play.db.jpa.Model;

@Entity
public class Periodos extends Model {

    public Long Periodo;
    public Integer Anio;
    @OneToOne
    public Estatus estatus;
    public String Nombre;
    public Date Fecha_inicio;
    public Date Fecha_fin;

    
    /**
     * 
     * @param Periodo       Indica el periodo de clases ej. Enero-Abril
     * @param Anio          Año del periodo 
     * @param estatus       Estatus
     * @param Nombre        Nombre del periodo
     * @param Fecha_inicio  En que fecha inicia dicho periodo
     * @param Fecha_fin     En que fecha termina dicho periodo
     */
    public Periodos(Long Periodo, Integer Anio, Estatus estatus, String Nombre, Date Fecha_inicio, Date Fecha_fin) {
        this.Periodo = Periodo;
        this.Anio = Anio;
        this.estatus = estatus;
        this.Nombre = Nombre;
        this.Fecha_inicio = Fecha_inicio;
        this.Fecha_fin = Fecha_fin;
    }

    public Long getPeriodo() {
        return Periodo;
    }

    public void setPeriodo(Long Periodo) {
        this.Periodo = Periodo;
    }

    public Integer getAnio() {
        return Anio;
    }

    public void setAnio(Integer Anio) {
        this.Anio = Anio;
    }

    public Estatus getEstatus() {
        return estatus;
    }

    public void setEstatus(Estatus estatus) {
        this.estatus = estatus;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public Date getFecha_inicio() {
        return Fecha_inicio;
    }

    public void setFecha_inicio(Date Fecha_inicio) {
        this.Fecha_inicio = Fecha_inicio;
    }

    public Date getFecha_fin() {
        return Fecha_fin;
    }

    public void setFecha_fin(Date Fecha_fin) {
        this.Fecha_fin = Fecha_fin;
    }
    
    

}
