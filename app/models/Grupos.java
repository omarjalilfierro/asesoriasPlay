package models;

import javax.persistence.*;
import play.data.validation.Required;
import play.db.jpa.Model;

/**
 * @author Administración de Proyectos Informáticos
 */
@Entity
public class Grupos extends Model {

    @Required
    public String Clave;
    @Required
    @OneToOne
    public Periodos Periodo;
    @Required
    @OneToOne
    public Carreras Carrera;
/**
 * 
 * @param Clave Identificador del grupo
 * @param Periodo Periodo que cursa
 * @param Carrera 
 */
    public Grupos(String Clave, Periodos Periodo, Carreras Carrera) {
        this.Clave = Clave;
        this.Periodo = Periodo;
        this.Carrera = Carrera;
    }

    
}
