package models;

import javax.persistence.Entity;
import play.db.jpa.Model;

/**
 *
 * Catalogo de carreras
 *
 * @author Administracion de Proyectos Informaticos
 * @since 2011
 * @version 1.0
 * @category Catalogo
 */
@Entity
public class Carreras extends Model {

    public String Clave;
    /**
     * 
     * @param Clave identificador de la carrera.
     */
    public Carreras(String Clave) {
        this.Clave = Clave;
    }
    

    public String Nombre() {
        return "Tecnologías de la Información y Comunicación";
    }

}
