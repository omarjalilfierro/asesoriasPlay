package models;

import java.util.List;
import javax.persistence.*;
import play.data.validation.Required;
import play.db.jpa.*;

@Entity
public class Alumnos extends Model {

    @Required
    @OneToOne(cascade = {CascadeType.ALL})
    private Personas Persona;
    @Required
    private String Matricula;
/**
 * Constructor de la clase alumno.
 * @param Persona Datos de Nombre y apellidos del alumno.
 * @param Matricula Numero entero de 11 cifras.
 */
    public Alumnos(Personas Persona, String Matricula) {
        this.Persona = Persona;
        this.Matricula = Matricula;
    } 

    public Personas getPersona() {
        return Persona;
    }

    public String getMatricula() {
        return Matricula;
    }

    public void setPersona(Personas Persona) {
        this.Persona = Persona;
    }

    public void setMatricula(String Matricula) {
        this.Matricula = Matricula;
    }
  
}
